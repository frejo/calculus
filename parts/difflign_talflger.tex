\part{Kapitel 7-9 -- Differentialligninger, talfølger og approksimation af funktioner}

\section{Differentialligninger}
    \subsection{Homogene anden ordens ligninger}
        \begin{equation}
            a y'' + b y' + cy = 0
        \end{equation}
        \begin{equation}
            D = b^2 - 4 a c
        \end{equation}
        \subsubsection{D > 0}
            To rødder:
            \begin{equation}
                r_1 = \frac{-b + \sqrt{D}}{2a}
            \end{equation}
            og
            \begin{equation}
                r_2 = \frac{-b - \sqrt{D}}{2a}
            \end{equation}

            Løsningen er så:
            \begin{equation}
                y(x) = A \e^{r_1x} + B \e^{r_2x}
            \end{equation}
        \subsubsection{D = 0}
            Én rod:
            \begin{equation}
                r = \frac{-b}{2a}
            \end{equation}

            Løsningen er så:
            \begin{equation}
                y(x) = A \e^{rx} + Bx\e^{rx}
            \end{equation}
        
        \subsubsection{D < 0}
            To komplekse rødder:
            \begin{equation}
                r_1 = \frac{-b + \sqrt{D}}{2a}
            \end{equation}
            og
            \begin{equation}
                r_2 = \frac{-b - \sqrt{D}}{2a}
            \end{equation}

            En fælles realdel:
            \begin{equation}
                k = \frac{-b}{2a}
            \end{equation}
            Så er
            \begin{equation}
                r_1 = k + i\omega
            \end{equation}
            og 
            \begin{equation}
                r_2 = k - i\omega
            \end{equation}
            hvor
            \begin{equation}
                \omega = \frac{\sqrt{4 a c - b^2}}{2a}
            \end{equation}

            Løsningen er Så
            \begin{equation}
                y(x) = A\e^{kx}\cos(\omega x) + B\e^{kx}\sin(\omega x)
            \end{equation}
    
    \subsection{Homogene ligninger af højere orden}
        Ligningen 
        \begin{equation}
            a_{n}y^{(n)} + a_{n-1}y^{(n-1)} + a_{n-2}y^{(n-2)} + ... + a_{1}y^{(1)} + a_{0}y^{(0)}
        \end{equation}
        har løsningen
        \begin{equation}
            y(x) = C_1y_1(x) + C_2y_2(x) + ... + C_ny_n(x)
        \end{equation}

        Hvor ligningen er faktoriseret ud, og hver faktor er en første eller andengradsligning, som er lette at finde roden for, og $C_ny_n(x)$ er løsningen for hver faktor.

        \begin{mycolbox}{gray}
            \textbf{Eksempel fra opgave 7.8}
            \vspace{1em}
            %
            Løs differentialligningen
            $$ y^{(4)} - 16y = 0 $$

            idet det oplyses, at 
            $$ z^4 - 16 = (z-2)(z+2)(z^2+4) $$
            %
            \seperator
            %
            Hver faktor giver en eller flere rødder:
            \begin{itemize}
                \item Fra $(z-2)$:
                    $$ r_1 = 2 $$
                \item Fra $(z+2)$:
                    $$ r_2 = -2 $$
                \item Fra $(z^2+4)$:
                    $$ a = 1, b = 0, c = 4 $$
                    $$ D = -16 $$
                    $$ r = \frac{\pm \sqrt{D}}{2} = \pm 2i $$
                    $$ k = 0, \omega = 2 $$
                    $$ r_3 = 2i $$
                    $$ r_4 = -2i $$
            \end{itemize}
            Løsningen er dermed
            \begin{equation*}
                y(x) = C_1\e^{2x} + C_2\e^{-2x} + C_3\cos(2x) + C_4\sin(2x)
            \end{equation*}

        \end{mycolbox}
    
    \subsection{Inhomogene ligninger}
        Hvis ligningen ikke går op i 0, altså
        \begin{equation}
            ay''+by'+cy=k
        \end{equation}
        bliver man nødt til at lede efter en \emph{partikulær løsning} og derefter finde den fuldstændige løsning til den homogene ligning (hvor $k = 0$). 

        For at finde en partikulær løsning, må man blive nødt til at gætte sig frem med mere eller mindre kvalificerede gæt. 

        \begin{mycolbox}{gray}
            \textbf{Eksempel fra opgave 7.14}
            \vspace{1em}
            Løs differentialligningen
            $$y^{(4)}-16y=p$$
            når $p(x) = 16x-32$
            %
            \seperator
            %
            Altså har vi ligningen
            $$ y^{(4)}-16y = 16x - 32 $$
            Hvis vi gætter på at $f_0$ er lineær, da $p$ er det, får vi at det altid vil gælde at ${f_0}^{(4)} = 0$. Dermed
            $$ -16 f_0 = 16x-32 $$
            $$ -f_0 = x-2 $$
            $$ f_0 = 2-x $$

            Det er altså et gæt på hvad $f_0$ kan være. Det kan vi så prøve at sætte ind i ligningen:
            $$ (2-x)^{(4)}-16(2-x)=16x-32 $$
            $$ 0 - 32 + 16 x = 16x-32 $$
            hvilket passer, og dermed er der fundet en partikulær løsning $f_0 = 2-x$.

            Den fuldstændige løsning for den homogene ligning er fundet i eksemplet ovenfor, til at være 
            \begin{equation*}
                y(x) = C_1\e^{2x} + C_2\e^{-2x} + C_3\cos(2x) + C_4\sin(2x)
            \end{equation*}

            Den fuldstændige løsning til den inhomogene ligning er så 
            \begin{equation*}
                y(x) = C_1\e^{2x} + C_2\e^{-2x} + C_3\cos(2x) + C_4\sin(2x) {\color{AUBlue} + 2-x}
            \end{equation*}
        \end{mycolbox}
    
    \subsection{Ikke-lineære differentialligninger og partielle differentialligninger}
        \subsubsection{Den logistiske ligning}
            Den logistiske differentialligning
            \begin{equation}
                y' = y(b-ay)
            \end{equation}
            eller
            \begin{equation}
                y'-y(b-ay)=0
            \end{equation}
            har løsningen $y(x)$ når
            \begin{equation}
                y'(x)=by(x)-ay(x)^2
            \end{equation}

            Funktionen
            \begin{equation}
                y(x) = \frac{b/a}{1+A\e^{-bx}}
            \end{equation}
            er en løsning til den logiske ligning.

            \begin{equation}
                \lim_{x\to\infty} y(x) = \frac{b}{a}
            \end{equation}
            \begin{equation}
                \lim_{x\to -\infty} y(x) = 0
            \end{equation}

        \subsubsection{Harmoniske ligninger}
            En differentialligning er harmonisk hvis den opfylder \emph{Laplace-ligningen}:
            \begin{equation}
                \pd[2]{f}{x}+\pd[2]{f}{y}=0
            \end{equation}
        \subsubsection{Bølgeligningen}
            Der findes en bølgeligning for ethvert reelt tal $c$:
            \begin{equation}
                \pd[2]{f}{x}=c^2\pd[2]{f}{y}
            \end{equation}

\section{Talfølger og uendelige rækker}
    Talfølger er en sekvens af tal, såsom
    \begin{equation}
        1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, ...
    \end{equation}
    som også kan skrives som 
    \begin{equation}
        \{n\}_{n=1}^\infty
    \end{equation}
    eller
    \begin{equation}
        1, \frac{1}{2}, \frac{1}{3},  \frac{1}{4},  \frac{1}{5},  \frac{1}{6},  \frac{1}{7},  \frac{1}{8},  \frac{1}{9},  \frac{1}{10}, \frac{1}{11}, \frac{1}{12}, ...
    \end{equation}
    som kan skrives som 
    \begin{equation}
        \left\{\frac{1}{n}\right\}_{n=1}^\infty
    \end{equation}

    \subsection{Konvergens}
        En talfølge $\{a_n\}$ er konvergent med grænseværdi $a$ hvis grænseværdien
        \begin{equation}
            \lim_{n\buzz}a_n = a
        \end{equation}
        findes.

        En talfølge, der ikke er konvergent er \emph{divergent}.

    \subsection{Regneregler for følger}
        \subsubsection{Kontinuær funktion}
            Hvis
            \begin{equation*}
                b_n = f(a_n)
            \end{equation*}
            og
            \begin{equation*}
                \lim_{n\buzz} a_n = a
            \end{equation*}
            er
            \begin{equation}
                \lim_{n\buzz} b_n = f(a)
            \end{equation}
        \subsubsection{Regneregler for to følger}
            For
            \begin{equation*}
                \lim_{n\buzz} a_n = a \text{ og } \lim_{n\buzz} b_n = b
            \end{equation*}
            gælder

            \begin{equation}
                \lim_{n\buzz} a_n + b_n = a + b
            \end{equation}
            \begin{equation}
                \lim_{n\buzz} a_n - b_n = a - b
            \end{equation}
            \begin{equation}
                \lim_{n\buzz} a_n b_n = ab
            \end{equation}
            \begin{equation}
                \lim_{n\buzz} \frac{a_n}{b_n} = \frac{a}{b}
            \end{equation}
            og for et tal $t\in\mathbb{C}$
            \begin{equation}
                \lim_{n\buzz} t a_n = ta
            \end{equation}

        \subsubsection{Kompleks følge}
            \begin{equation}
                \lim_{n\buzz}z_n = 
                \lim_{n\buzz} \text{Re} (z_n) + 
                i \lim_{n\buzz} \text{Im} (z_n)
            \end{equation}
        
    \subsection{Betegnelser}
        \subsubsection{Voksende}    
            \begin{equation}
                a_n \leq a_{n+1}
            \end{equation}
        \subsubsection{Aftagende}    
            \begin{equation}
                a_n \geq a_{n+1}
            \end{equation}
        \subsubsection{Monoton}
            Enten voksende eller aftagende:
            \begin{equation}
                a_n \leq a_{n+1} \cup a_n \geq a_{n+1}
            \end{equation}
        \subsubsection{Begrænset}
            En følge er begrænset når der findes et positivt tal, som er større end $|a_n|$ for alle $n$.
    
    \subsection{Talrækker}
        En talrække er summen af en følge:
        \begin{equation}
            1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 + 11 + 12 + ...
        \end{equation}
        som også kan skrives som
        \begin{equation}
            \sum_{n=0}^\infty n
        \end{equation}
    \subsection{Konvergens for talrækker}
        \subsubsection{Geometriske rækker}
            For $z\in\mathbb{C}$ hvis $|z| < 1$ vil
            \begin{equation}
                \sum_{n=0}^\infty z^n = 1 + z + z^2 + z^3 + ...
            \end{equation}
            konvergere mod sum
            \begin{equation}
                \sum_{n=0}^\infty z^n = \frac{1}{1-z}
            \end{equation}

        \subsubsection{Konvergenskriteriet}
            En uendelig række $\sum_{n=0}^\infty a_n$ kan kun være konvergent hvis $\lim_{n\buzz}a_n = 0$.
        
        \subsubsection{Sammenligningskriteriet}
            $\{a_n\}_{n=0}^\infty$ og $\{b_n\}_{n=0}^\infty$ er følger af positive tal. Når for alle tal
            \begin{equation}
                a_n \leq b_n
            \end{equation}
            gælder
            \begin{enumerate}
                \item Hvis $\sum_{n=0}^\infty b_n$ er konvergent, er $\sum_{n=0}^\infty a_n$ det også.
                \item Hvis $\sum_{n=0}^\infty a_n$ er divergent, er $\sum_{n=0}^\infty b_n$ det også.
            \end{enumerate}
        
        \subsubsection{Integralkriteriet}
            Hvis $f : [1,\infty[\to[0,\infty[$ er kontinuær og aftagende, så er rækken
            \begin{equation}
                \sum_{n=1}^\infty f(n)
            \end{equation}
            konvergent hvis
            \begin{equation}
                \left\{ \int_1^k f(x) \dif x \right\}_{k=1}^\infty
            \end{equation}
            er konvergent.
    
    \subsection{Alternerende rækker}
        Leddene hele tiden skifter fortegn. Eksempel:
        \begin{equation}
            \sum_{n=1}^\infty (-1)^n\frac{1}{n} = -1 + \frac{1}{2} - \frac{1}{3} + \frac{1}{4} - \frac{1}{5} + ...
        \end{equation}
        \subsubsection{Konvergens for alternerende rækker}
            En alternerende række hvor
            \begin{equation}
                |a_n|\geq|a_{n+1}
            \end{equation}
            er kun konvergent hvis
            \begin{equation}
                \lim_{n\buzz}a_n=0
            \end{equation}
    \subsection{Absolut konvergens}
        En uendelig række er absolut konvergent, hvis
        \begin{equation}
            \sum_{n=0}^\infty |a_n|
        \end{equation}
        er konvergent.

        Udover at en absolut konvergent række er konvergent, så så gælder der at
        \begin{equation}
            \Big| \sum_{n=0}^\infty a_n \Big| \leq \sum_{n=0}^\infty |a_n|
        \end{equation}

\section{Approksimering af funktioner}
    \subsection{Potensrækker}
        For enhver potensrække
        \begin{equation}
            \sum_{n=0}^\infty a_nx^n
        \end{equation}
        findes et $R\geq0$, såpotensrækken konvergerer når $|x| < R$ og divergerer når $|x| > R$

        R kaldes for potensrækkens konvergensradius. 
        En potensrække vil altid konvergere når $x=0$. 
        Hvis $R = \infty$ vil potensrækken konvergere for alle tal $x$.

        \subsubsection{Konvergensradius}
            konvergensradius kan ofte findes som
            \begin{equation}
                R = \lim_{n\buzz}\left|\frac{a_n}{a_{n+1}}\right|
            \end{equation}
        
        \subsubsection{Differentiation af potensrække}
            Hvis $R > 0$ for potensrækken $\sum_{n=0}^\infty a_nx^n$ og $f$ er sumfunktionen, så er $f$ differentabel og 
            \begin{equation}
                f'(x) = \sum_{n=1}^\infty na_nx^{n-1}
            \end{equation}
            hvilken også kan skrives som en anden potensrække
            \begin{equation}
                \sum_{n=0}^\infty b_nx^{n}
            \end{equation}
            hvor $b_n = (n+1)a_{n+1}$. Konvergensradius vil være den samme for den oprindelige potensrække og denne. 
        
        \subsubsection{Integration af potensrække}
            \begin{equation}
                \int\sum_{n=0}^\infty a_nx^n \dif x = \sum_{n=0}^\infty \frac{a_n}{n+1}x^{n+1}
            \end{equation}

    \subsection{Taylorrækker}
        \begin{equation}
            \sum_{n=0}^\infty\frac{f^{(n)}(0)}{n!}x^n
        \end{equation}
        er potensrækken, der har $f$ som sumfunktion, med udviklingspunkt i 0.

        Er funktionen ikke defineret i eller ikke uendeligt ofte differentabel i 0, men et andet tal $a$, kan man flytte udviklingspunktet:
        \begin{equation}
            \sum_{n=0}^\infty\frac{f^{(n)}(a)}{n!}(x-a)^n
        \end{equation}
    
    \subsection{Taylor's restledsvurdering}
        Lad $R_k(x)$ være forskellen mellem $f$ og sumfunktionen:
        $$ R_k(x) = f(x) - \sum_{n=0}^k\frac{f^{(n)}(a)}{n!}(x-a)^n $$
        så er 
        $$ f(x) = \sum_{n=0}^\infty\frac{f^{(n)}(a)}{n!}(x-a)^n $$
        kun hvis 
        $$ \lim_{k\buzz} R_k(x) = 0 $$

        $R_k(x)$ kaldes for det $k$'te restled.

        \subsubsection{Bestemmelse om en funktion er sumfunktionen af taylorrække}
        Man kan bruge følgende funktion til at bestemme om en funktion er sumfunktionen af dens taylorrække, og få en vurdering af hvor stor en fejl, man begår når man approksimerer funktionen med taylorrækken. 

        Hvis $M$ er et positivt tal der opfylder at $|f^{k+1}(x)|\leq M$ når $|x-a|\leq d$, så er
        \begin{equation}
            |R_k(x)| \leq M\frac{|x-a|^{k+1}}{(k+1)!}
        \end{equation}
        når $|x-a|\leq d$.

        Den maksimale fejl, er 
        $$ M\frac{|x-a|^{k+1}}{(k+1)!} $$
